import {AccessRecord, AccessType, canPerformAction} from "../AccessDomain";
import {User} from "../../recipients/UserDomain";
import {Company} from "../../recipients/CompanyDomain";

const user: User = {
    id: 'u01',
    companyId: 'does not matter',
};

const company: Company = {
    id: 'c01',
};

const action1 = 'DO_ACTION';
const action2 = 'DO_ANOTHER_ACTION';
const object1Id = 'o01';
const object2Id = 'o02';

const testAccessRights: AccessRecord[] = [
    new AccessRecord({
        recipient: user,
        action: action1,
        objectId: object1Id,
        accessType: AccessType.Full,
    }),
    new AccessRecord({
        recipient: company,
        action: action2,
        accessType: AccessType.Edit,
        objectId: null,
    }),
    new AccessRecord({
        recipient: user,
        action: action2,
        accessType: AccessType.Read,
        objectId: object2Id,
    }),
];

test('Empty available rights does not allow to perform action', () => {
    expect(canPerformAction([], action1, AccessType.Read, object1Id)).toBeFalsy();
})

test('Full rights user can perform edit action', () => {
    expect(canPerformAction(testAccessRights, action1, AccessType.Edit, object1Id)).toBeTruthy();
})

test('User rights has higher priority than company', () => {
    expect(canPerformAction(testAccessRights, action2, AccessType.Edit, object2Id)).toBeFalsy();
    expect(canPerformAction(testAccessRights, action2, AccessType.Read, object2Id)).toBeTruthy();
})