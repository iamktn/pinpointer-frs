import {ObjectID} from "../common/Types";
import {User, UserRole} from "../recipients/UserDomain";
import {Company} from "../recipients/CompanyDomain";

export type RecordID = string;
export type RecipientID = string;

export enum AccessType {
    Read,
    Edit,
    Full,
}

export interface Recipient {}

export class AccessRecord {
    recipient: Recipient | null;
    action: string;
    objectId: RecordID | null;
    accessType: AccessType;
    // This one could be extended: add field access and different actions

    constructor(data: Omit<AccessRecord, "priority">) {
        Object.assign(this, data);
    }

    // Just an example of right apply priority. It could be moved somewhere else - in the clean function
    get priority(): number {
        // Empty recipient means role rights
        if (!this.recipient) return 3;
        if (this.recipient instanceof User) return 2;
        if (this.recipient instanceof Company) return 1;
        return 0;
    }
}

export function canPerformAction(accessRights: AccessRecord[],
                                 action: string,
                                 accessType: AccessType,
                                 objectId: ObjectID | null = null): boolean {
    const affectedRights = accessRights.filter(
        r => !objectId || r.objectId === objectId &&
        r.action == action
    ).sort((a, b) => a.priority - b.priority);

    if (affectedRights.length === 0) return false;

    return affectedRights[0].accessType >= accessType;
}

export function getRoleAccessRights(role: UserRole): AccessRecord[] {
    // Here we can add fixed rights for user roles
    switch (role) {
        case UserRole.Admin:
            return [];
        case UserRole.Employee:
            return [];
        case UserRole.ExternalReceiver:
            return [];
        default:
            return [];
    }
}
