import {AccessRecord, AccessType, RecipientID} from "./AccessDomain";
import * as UserService from '../recipients/UserService';
import * as CompanyService from '../recipients/CompanyService';
import {IRecipient} from "../recipients/Types";
import * as AccessRepository from "./AccessRepository";
import * as AccessDomain from "./AccessDomain";
import {ObjectID} from "../common/Types";

async function getRecipientsForUser(id: RecipientID): Promise<Array<IRecipient>> {
    const user = await UserService.get(id);
    const companies = await CompanyService.getCompaniesForUser(id);

    return [ user, ...companies];
}

// This one will be used in middleware and it will be passed separate in IGqlContext
export async function getAccessRightsForUser(id: RecipientID): Promise<AccessRecord[]> {
    const user = await UserService.get(id);
    const roleRights = AccessDomain.getRoleAccessRights(user.role);

    const recipients = await getRecipientsForUser(id);
    const ids = recipients.map(r => r.id);

    return [...roleRights, ...(await AccessRepository.getAccessDataForUser(ids))];
}

export async function grant(recipient: IRecipient, action: string, AccessType, id: ObjectID) {
    await AccessRepository.addAccessRecord(recipient.id, action, id);
}

export function checkAbility(accessRights: AccessRecord[], action: string, accessType: AccessType, objectId: ObjectID | null) {
  if (!AccessDomain.canPerformAction(accessRights, action, accessType, objectId)) throw new Error("Permission error");
}