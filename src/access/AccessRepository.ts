// recipients list
import {AccessRecord, AccessType, RecipientID, RecordID} from "./AccessDomain";
import {UserDoc} from "../recipients/UserDoc";
import {CompanyDoc} from "../recipients/CompanyDoc";
import * as UserRepository from "../recipients/UserRepository";
import * as CompanyRepository from "../recipients/CompanyRepository";
import {ObjectID, ObjectTypeID} from "../common/Types";


interface AccessRecordDoc {
    id: RecordID,
    recipient: UserDoc | CompanyDoc,
    action: string,
    objectID: ObjectID,
    accessType: AccessType,
}

function mapper(doc: AccessRecordDoc): AccessRecord {
    if (doc.recipient instanceof UserDoc) {
        return new AccessRecord({
            recipient: UserRepository.mapper(doc.recipient as UserDoc),
            action: doc.action,
            objectId: doc.objectID,
            accessType: doc.accessType,
        });
    } else if (doc.recipient instanceof CompanyDoc) {
        return new AccessRecord({
            recipient: CompanyRepository.mapper(doc.recipient as CompanyDoc),
            action: doc.action,
            objectId: doc.objectID,
            accessType: doc.accessType,
        });

    } else throw new Error();
}

export async function getAccessDataForUser(recipients: RecipientID[],
                                           objectTypeID: ObjectTypeID | null = null,
                                           objectID: ObjectID | null = null): Promise<AccessRecord[]> {
    // We should populate here recipients from two collections
    const queryResult: AccessRecordDoc[] = [];

    return queryResult.map(r => mapper(r));
}

export async function addAccessRecord(recipient: RecipientID, action: string, objectID: ObjectID | null = null) {
    // Save into db
}