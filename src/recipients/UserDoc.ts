import {dbID, IRecipientDoc} from "./Types";
import {UserRole} from "./UserDomain";

export class UserDoc implements IRecipientDoc {
    id: dbID;
    companyId: dbID | null;
    role: UserRole;
}