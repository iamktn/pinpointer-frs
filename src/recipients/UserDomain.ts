import {IRecipient} from "./Types";
import {RecipientID} from "../access/AccessDomain";

export enum UserRole {
    Admin,
    Employee,
    ExternalReceiver
}

export class User implements IRecipient {
    id: RecipientID;
    companyId: RecipientID | null;
    role: UserRole;
    // Rest fields

    constructor(id: RecipientID, companyId: RecipientID, role: UserRole) {
        this.id = id;
        this.companyId = companyId;
        this.role = role;
    }
}