import {RecipientID} from "../access/AccessDomain";

export type dbID = string;

export interface IRecipient {
    id: RecipientID;
}

export interface IRecipientDoc {
    id: dbID;
}