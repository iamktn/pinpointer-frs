import {ObjectID} from "../common/Types";
import {IRecipient} from "./Types";

export class Company implements IRecipient {
    id: ObjectID;
    // Rest fields

    constructor(id: ObjectID) {
        this.id = id;
    }
}