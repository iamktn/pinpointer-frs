import {CompanyDoc} from "./CompanyDoc";
import {Company} from "./CompanyDomain";

export function mapper(doc: CompanyDoc): Company {
    return new Company(doc.id);
}