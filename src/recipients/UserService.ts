import {User, UserRole} from "./UserDomain";
import {RecipientID} from "../access/AccessDomain";

export function get(userId: RecipientID) {
    const defaultCompanyId:  RecipientID = 'ed71a036-6878-11ec-90d6-0242ac120003';
    // Here should be repository call with populate and mapping
    return new User(userId, defaultCompanyId, UserRole.Admin);
}