import {User} from "./UserDomain";
import {UserDoc} from "./UserDoc";

export function mapper(doc: UserDoc): User {
    return new User(doc.id, doc.companyId, doc.role);
}