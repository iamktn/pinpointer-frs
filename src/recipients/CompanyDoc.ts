import {dbID, IRecipientDoc} from "./Types";

export class CompanyDoc implements IRecipientDoc {
    id: dbID;
}