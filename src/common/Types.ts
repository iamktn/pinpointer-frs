import {AccessRecord} from "../access/AccessDomain";
import {User} from "../recipients/UserDomain";

export type ObjectTypeID = string;
export type ObjectID = string;

export interface IGqlContext {
    user: User;
    accessRights: AccessRecord[];
}
