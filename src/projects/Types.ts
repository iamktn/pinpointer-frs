export interface IProjectCreateInput {
    id: string;
    name: string;
}