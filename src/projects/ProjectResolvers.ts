// This is not really resolvers, just way to show access usage

import {IGqlContext} from "../common/Types";
import * as AccessService from "../access/AccessService";
import {AccessType} from "../access/AccessDomain";
import * as ProjectService from "./ProjectService";
import {Project} from "./ProjectService";
import {IProjectCreateInput} from "./Types";

const resolvers = {
    Project: {

    },
    Query: {
        project: async (_: any, { id }: { id: string }, { user, accessRights }: IGqlContext) => {
            AccessService.checkAbility(accessRights, "PROJECT", AccessType.Read, id);

            return ProjectService.get(id);
        }
    },
    Mutation: {
       projectCreate: async (_: any, { input }: { input: IProjectCreateInput }, { user, accessRights }: IGqlContext) => {
           AccessService.checkAbility(accessRights, "PROJECT", AccessType.Edit, input.id);

           return ProjectService.create(user, input);
       }
    }
}
