import {ObjectID} from "../common/Types";
import {User} from "../recipients/UserDomain";
import {IProjectCreateInput} from "./Types";
import * as AccessService from "../access/AccessService";
import * as CompanyService from "../recipients/CompanyService";
import {AccessType} from "../access/AccessDomain";

export class Project {
    id: ObjectID;

    constructor(id: ObjectID) {
        this.id = id;
    }
}

export async function get(id: ObjectID): Promise<Project> {
    return new Project(id);
}

export async function create(user: User, input: IProjectCreateInput): Promise<Project> {
    // Common creation routine - check input and so on
    // ...

    await AccessService.grant(user, "PROJECT", AccessType.Edit, input.id);

    const companies = await CompanyService.getCompaniesForUser(user.id);

    for (const company of companies) {
        await AccessService.grant(company, "PROJECT", AccessType.Read, input.id);
    }

    return get(input.id);

}